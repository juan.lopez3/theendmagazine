<?php 
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}


function woo_button_color_style() {
$plugin_url = plugin_dir_url( __FILE__ );
wp_enqueue_style( 'woobuttonstyle', $plugin_url . 'styles/woobuttonstyle.php' );
?>
<style type="text/css" id="twentytwelve-admin-header-css">
.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
background:<?php echo get_option('woo_button_color');?>!important;
border-color:<?php echo get_option('woo_button_color');?>!important;
color:<?php echo get_option('woo_button_text_color');?>!important;
border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-moz-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-webkit-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-o-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
}

.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button{
background:<?php echo get_option('woo_button_color');?>!important;
border-color:<?php echo get_option('woo_button_color');?>!important;
color:<?php echo get_option('woo_button_text_color');?>!important;
border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-moz-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-webkit-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-o-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
}


</style>
<?php
}
add_action( 'wp_enqueue_scripts', 'woo_button_color_style' );

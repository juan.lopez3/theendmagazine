<style type="text/css" id="twentytwelve-admin-header-css">
.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
background:<?php echo get_option('woo_button_color');?>!important;
border-color:<?php echo get_option('woo_button_color');?>!important;
color:<?php echo get_option('woo_button_text_color');?>!important;
border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-moz-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-webkit-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-o-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
}

.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button{
background:<?php echo get_option('woo_button_color');?>!important;
border-color:<?php echo get_option('woo_button_color');?>!important;
color:<?php echo get_option('woo_button_text_color');?>!important;
border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-moz-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-webkit-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
-o-border-radius:<?php echo get_option('woo_button_border_radius');?>px!important;
}
</style>
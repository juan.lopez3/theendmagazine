<?php
//when called directly @ added version 2.0.2
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
//Add to cart Button name on archive page
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_button_changer_archive_page' );    // 2.1 +
 
function woo_button_changer_archive_page() {

  return __( get_option('woo_button_text_archive'),$woo_default_value, 'woocommerce' );
 }
 
//Add to cart Button name on single product page
                    
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text_single_product');

//Add to cart Button name on archive page
 function woo_custom_cart_button_text_single_product() {
  return __( get_option('woo_button_text_single'),$woo_default_value, 'woocommerce' );
}  
  //Woo Version <2.1   
 add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' );   //Woo Version <2.1  
 function woo_custom_cart_button_text() {
 
   return __( get_option('woo_button_text_archive_old'),$woo_default_value, 'woocommerce' );
} 
//Adding Style Sheet

include('styles/woobuttonstyle.php');

//=========Creating The Menu===============
add_action('admin_menu', 'baw_create_menu');

function baw_create_menu() {
add_menu_page('Button Text Settings', 'Woo Button Text', 'administrator', 'woo-button-text', 'baw_settings_page' );
add_submenu_page('woo-button-text', 'Button Color Settings', 'Button Color & shape', 'administrator','woo-button-color','woo_button_color_page' );
//add_submenu_page('woo-button-text', 'Button Format Settings', 'Button Format', 'administrator','woo-button-format', 'woo_button_format_page' );

//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
}

//===Calling submenu=======
function woo_button_color_page() {
echo  '<h2>Woo Commerce Add to Cart Button Style</h2>';
include('inc/color.php');
}

function woo_button_format_page() {

echo 'I am woo button format page';
include('inc/format.php');
}
// create custom plugin settings menu
function register_mysettings() {
	//register our settings
	register_setting( 'baw-settings-group', 'woo_button_text_archive' );
	register_setting( 'baw-settings-group', 'woo_button_text_single' );
	register_setting( 'baw-settings-group', 'woo_button_text_archive_old' );
	register_setting( 'woo-settings-group', 'woo_button_color' );
	register_setting( 'woo-settings-group', 'woo_button_text_color' );
	register_setting( 'woo-settings-group', 'woo_button_border_radius' );
	//register_setting( 'woo-settings-group', 'woo_button_type' );
}
//Adding Defaults value for these field!
$woo_default_value = 'Add to Cart';
//

function baw_settings_page() {
global $woo_default_value;
?>
<div class="wrap">

<?php
if ($_GET['settings-updated']==true) { ?>
<div id="setting-error-settings-updated" class="updated settings-error"> 
<p><strong>Settings saved.</strong></p></div>
<?php } ?>
<?php $weburl = home_url(); ?>
<div id="icon-tools" class="icon32"></div><h2>Woo Commerce Change add to cart button text <a class="woo-link" href="<?php echo $weburl;?>/wp-admin/admin.php?page=woo-button-color"> Change Button Color</a> </h2>

<form method="post" action="options.php">
    <?php settings_fields( 'baw-settings-group' ); ?>
    <?php do_settings_sections( 'baw-settings-group' ); ?>
    <table class="form-table">
		
		<tr valign="top">
        <th scope="row">Button Text Shop Page WooCommerce 2.1+ </th>
        <td><input type="text" name="woo_button_text_archive" value="<?php echo esc_attr( get_option('woo_button_text_archive',$woo_default_value) ); ?>" required/></td>
        </tr>
		
	    <tr valign="top">
        <th scope="row">Button Text Single Product Page</th>
        <td><input type="text" name="woo_button_text_single" value="<?php echo esc_attr( get_option('woo_button_text_single',$woo_default_value) ); ?>" required/></td>
        </tr>
		
	    <tr valign="top">
        <th scope="row">Button Text For older  WooCommerce < 2.1 </th>
        <td><input type="text" name="woo_button_text_archive_old" value="<?php echo esc_attr( get_option('woo_button_text_archive_old',$woo_default_value) ); ?>" required/></td>
        </tr>
   
    </table>
    
    <?php submit_button(); ?>

</form>



<style>

.woo-link{
    background: #5F4A89;
  padding: 5px;
  color: #fff;
  text-decoration: none;
  border-radius: 5px;
}

.fb-like{
  background: #FFF;
  padding: 4px;
  width: 35%;

}
.dev-credit{
  background: #C4EBFF;
  padding: 10px;
  width: 350px;
  border-radius: 10px;
  color: #0022FF;
}
</style>
<div class="dev-credit">

<!--Facebook Like Button-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=250636638419749";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--Facebook Like Button-->

Develop By <a href="http://fiverr.com/rupomkhondaker" target="_blank">Rupom Khondaker</a><br>
For More details Please Visit <a href="http://www.exclutips.com/plugins/woo-button-text/" target="_blank">Visit Plugin Page</a><br /><br />
Please Like & Share to Support this Plugin: <div class="fb-like" data-href="http://www.facebook.com/exclutips" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
</div>
</div>
<?php } ?>
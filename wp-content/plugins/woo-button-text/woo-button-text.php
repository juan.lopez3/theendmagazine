<?php
/**
* Plugin Name: Woo Button Text.
* Plugin URI: http://www.exclutips.com/plugins/woo-button-text/
* Description: Woo Commerce Button Text or Add to cart button text changer with Button Color styler. 
* Version: 2.0.3
* Author: Rupom Khondaker
* Author URI: http://fiverr.com/rupomkhondaker
/*Copyright 2015 Rupom Khondaker (email: rupomkhondaker at gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
?>
<?php
//when called directly  @ added version 2.0.2
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/*Checking Woo Commerce Plugin installed or not.
@ added version 2.0.2
*/
add_action( 'admin_init', 'check_woocommerce_installed' );
function check_woocommerce_installed() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        add_action( 'admin_notices', 'woo_plugin_notice' );
        deactivate_plugins( plugin_basename( __FILE__ ) ); 
        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function woo_plugin_notice(){
?>
<div class="error">
<p>Sorry, but <strong>Woo Button Text</strong> Plugin requires the Woo Commerce
 plugin to be installed and active. 
 You can <a href="https://wordpress.org/plugins/woocommerce/">download </a>it 
 from <a href="https://wordpress.org/plugins/woocommerce/">Woo Commerce</a></p></div>
<?php }

/*Add a setting link to plugin list page
@ added version 2.0.2
*/
function plugin_add_settings_link( $links ) {
    $woo_button_settings = '<a href="admin.php?page=woo-button-text">' . __( 'Settings' ) . '</a>';
    array_push( $links, $woo_button_settings );
  	return $links;
}
$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'plugin_add_settings_link' );

// Call Main Plugin page
require_once ('functions.php');
?>

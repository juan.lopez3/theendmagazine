<?php
// when called directly @ added version 2.0.2
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

if ($_GET['settings-updated']==true) { ?>
<div id="setting-error-settings-updated" class="updated settings-error"> 
<p><strong>Button Style Updated !!!.</strong></p></div>
<?php } ?>



<form method="post" action="options.php">
    <?php settings_fields( 'woo-settings-group' ); 
     do_settings_sections( 'woo-settings-group' );
		wp_enqueue_style( 'wp-color-picker' );          
		wp_enqueue_script( 'wp-color-picker' ); 
	?>
<table class="form-table">
	
	<tr valign="top">
	<th scope="row">Button Background Color: </th>
	<td><input type="text" id="button" name="woo_button_color" value="<?php echo esc_attr( get_option('woo_button_color','#00008B') ); ?>" class="my-color-picker" data-default-color="#00008B" /></td>
	</tr>
	
	<tr valign="top">
	<th scope="row">Button Text Color: </th>
	<td><input type="text" id="text-color" name="woo_button_text_color" value="<?php echo esc_attr( get_option('woo_button_text_color','#FFFAF0') ); ?>" class="my-color-picker" data-default-color="#ffffff" /></td>
	</tr>
	
	<tr valign="top">
	<th scope="row">Button Border Radius: </th>
	<td><input type="number" id="border-radius" min="0" max="15" name="woo_button_border_radius" value="<?php echo esc_attr( get_option('woo_button_border_radius','3') ); ?>" /></td>
	</tr>
	<!--
	<tr valign="top">
	<th scope="row">Select Button Type:</th>
	<td>
	 <?php //$selected = get_option('woo_button_type');?>
        <select id="button-type" name="woo_button_type" class="button-format" value="<?php //echo esc_attr( get_option('woo_button_type','1') ); ?>" >
			    <option value="1" <?php //if($selected == '1'){echo("selected");}?>>wooCommerce</option>
                <option value="2" <?php //if($selected == '2'){echo("selected");}?>>3d</option>
                <option value="3" <?php //if($selected == '3'){echo("selected");}?>>flat</option>
				
        </select>
	</td>
	</tr>-->
	
	</table>
    <?php submit_button(); ?>
</form>
<script type='text/javascript'>  
    jQuery(document).ready(function($) {  
        $('.my-color-picker').wpColorPicker();  
    });  
</script>

<style>

.woo-link{
    background: #5F4A89;
  padding: 5px;
  color: #fff;
  text-decoration: none;
  border-radius: 5px;
}
.fb-like{
  background: #FFF;
  padding: 4px;
  width: 35%;

}
.dev-credit{
  background: #C4EBFF;
  padding: 10px;
  width: 350px;
  border-radius: 10px;
  color: #0022FF;
}
</style>
<div class="dev-credit">

<!--Facebook Like Button-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=250636638419749";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--Facebook Like Button-->

Develop By <a href="http://fiverr.com/rupomkhondaker" target="_blank">Rupom Khondaker</a><br>
For More details Please Visit <a href="http://www.exclutips.com/plugins/woo-button-text/" target="_blank">Visit Plugin Page</a><br /><br />
Please Like & Share to Support this Plugin: <div class="fb-like" data-href="http://www.facebook.com/exclutips" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
</div>